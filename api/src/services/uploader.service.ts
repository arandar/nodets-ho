import { MongoService, Service } from 'typespring';
import * as path from 'path';
import * as fs from 'file-system';
import * as uuid from 'uuid';
import * as sharp from 'sharp';
import { API_ERROR } from '../models/constants/errors';

@Service()
export class UploaderService {

    constructor(private mongo: MongoService) {
    }

    async uploadImage(file: any): Promise<string> {
        if (!file) return Promise.reject(API_ERROR.BAD_REQUEST);
        const filePath = path.join(__dirname, '..', 'assets', 'public');

        if (!fs.existsSync(filePath)) fs.mkdirSync(filePath);

        try {
            const img = sharp(file.data);
            const fPath = `${filePath}`;
            let fName;
            const metadata = await img.metadata();

            switch (metadata.format) {
                case 'jpeg':
                    fName = `${uuid.v1()}.jpeg`;
                    await Promise.all(
                        [
                            img.jpeg().toFile(`${fPath}/${fName}`),
                            img.jpeg().resize(1920, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${fPath}/lg-${fName}`),
                            img.jpeg().resize(1280, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${fPath}/md-${fName}`),
                            img.jpeg().resize(768, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos3,
                                }).toFile(`${fPath}/sm-${fName}`),
                        ],
                    );
                    break;
                case 'png':
                    fName = `${uuid.v1()}.png`;
                    await Promise.all(
                        [
                            img.png().toFile(`${fPath}/${fName}`),
                            img.png().resize(1920, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos2,
                                }).toFile(`${fPath}/lg-${fName}`),
                            img.png().resize(1280, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos2,
                                }).toFile(`${fPath}/md-${fName}`),
                            img.png().resize(768, null,
                                {
                                    withoutEnlargement: true,
                                    kernel: sharp.kernel.lanczos2,
                                }).toFile(`${fPath}/sm-${fName}`),
                        ],
                    );
                    break;
                default:
                    return Promise.reject(API_ERROR.BAD_REQUEST);
            }
            return Promise.resolve(fName);
        } catch (e) {
            console.error(e);
            return Promise.reject(e);
        }
    }
}