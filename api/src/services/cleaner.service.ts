import { Collections } from './../models/enums/collections';
import * as cron from 'node-cron';
import * as fs from 'file-system';
import * as path from 'path';
import { Service, MongoService, LoggerFactory } from 'typespring';
import { Logger } from 'winston';

@Service()
export class CleanerService {
    private logger: Logger;
    private cleaningPeriod = '0 0 * * *'; // cleaning files every day
    // everyFiveMinutes '*/5 * * * *';
    // every 30 seconds '30 * * * * *';
    private FOLDER = path.join(__dirname, '..', 'assets', 'public');

    constructor(
        private mongo: MongoService,
        loggerFactory: LoggerFactory, // give informations
    ) {
        this.logger = loggerFactory.getLogger('FILE-CLEANER');
        this.runFileCleaner();
    }

    runFileCleaner(): void {
        cron.schedule(this.cleaningPeriod, async () => {
            this.logger.info(`Running file cleaner task at ${new Date()}`);
            this.CheckFilesFolder();
        });
    }

    async CheckFilesFolder(): Promise<any> {
        this.logger.info('Checking img files...');
        const dirPath = `${this.FOLDER}`;
        let files: string[];
        try {
            files = fs.readdirSync(dirPath);
        } catch (error) {
            this.logger.error(`Loading files error: ${error}`);
            return;
        }
        if (!files.length) {
            this.logger.info('No files found...');
            return;
        }
        const collectionFiles = await this.getFiles();

        files.forEach(file => {
            if (file === '.gitkeep') return;
            const f = (
                file.indexOf('lg-') === 0 ||
                file.indexOf('md-') === 0 ||
                file.indexOf('sm-') === 0) ? `http://localhost:3000/public/${file.substring(3)}` :
                `http://localhost:3000/public/${file}`;
            if (!collectionFiles.includes(f)) {
                console.log('Unused file found: ', file);
                try {
                    fs.unlinkSync(`${dirPath}/${file}`);
                } catch (error) {
                    this.logger.error(`Deleting unused public file error: ${error}.`);
                }
            }
        });
        this.logger.info('Deleting unused files success.');
    }

    async getFiles(): Promise<string[]> {
        const img: any = await this.mongo.collection(Collections.Cars).find({}).toArray();
        const imgFiles = img.map(i => i.img);
        return imgFiles;
    }
}