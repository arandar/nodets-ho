import { Request } from 'express';
import { Controller, GetRequest, LoggerFactory, PostRequest, PutRequest, DeleteRequest } from 'typespring';
import { Logger } from 'winston';
// import { ICarShop } from '../models/interfaces/car-shop';
// import { carShops } from '../models/constants/cars.const';
import { CarsRepository } from '../repository/cars';
import { ICar } from '../models/interfaces/car';
import { UploaderService } from '../services/uploader.service';

@Controller('/cars')
export class CarsController {
    private _logger: Logger;

    constructor(
        loggerFactory: LoggerFactory,
        private carsRepo: CarsRepository,
        private uploaderService: UploaderService,
    ) {
        this._logger = loggerFactory.getLogger('TEST');
    }

    // @GetRequest('/')
    // async getTest(req: Request): Promise<ICarShop[]> {
    //     this._logger.info('Requesting cars');
    //     // return [];
    //     return carShops;
    // }
    // вывод всех машин по гет запросу
    @GetRequest('/')
    async getCars(req: Request): Promise<ICar[]> {
        this._logger.info('Requesting cars');
        // return [];
        // return carShops;
        return await this.carsRepo.getCars();
    }

    @PostRequest('/img')
    async uploadImage(req: Request): Promise<string> {
        return this.uploaderService.uploadImage(req.files.file);
    }

    // добавление машины по пост
    @PostRequest('/')
    async addCar(req: Request): Promise<string> {
        return await this.carsRepo.addCar(req.body.car, req.body.shopId);
    }

    // редактирование через пут запрос
    @PutRequest('/')
    async updateCar(req: Request): Promise<void> {
        return await this.carsRepo.updateCar(req.body);
    }

    // удаление через делит запрос
    @DeleteRequest('/:id')
    async deleteCar(req: Request): Promise<void> {
        return await this.carsRepo.deleteCar(req.params.id);
    }
}