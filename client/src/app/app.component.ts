import { LoadCarsAction } from './store/cars/cars.action';
import { AppState } from './store/index';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { LoadCarShopsAction } from './store/car-shops/car-shops.action';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    constructor(
        private store: Store<AppState>,
    ) {
        this.store.dispatch(new LoadCarShopsAction());
        this.store.dispatch(new LoadCarsAction());
    }
}
