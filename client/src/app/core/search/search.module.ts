import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search.component';
import { FormsModule } from '@angular/forms';
import { PipeModule } from '../../shared/modules/pipe.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PipeModule,
    ],
    declarations: [SearchComponent],
    exports: [SearchComponent],
})
export class SearchModule { }
