import { ISearch } from './../../shared/models/interfaces/search';
import { AppState } from './../../store/index';
import { Store } from '@ngrx/store';
import { getCarShopsStoreState } from './../../store/car-shops/car-shops.reducer';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, combineLatest } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { getCarsStoreState } from '../../store/cars/cars.reducer';

@Component({
    selector: 'car-test-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
    carSearch$: Observable<ISearch[]>;
    searchString: string = '';

    constructor(
        private store: Store<AppState>,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.carSearch$ = combineLatest(
            this.store.select(getCarsStoreState).pipe(
                map(s => {
                    if (!s.cars || !s.cars.length) return;
                    return s.cars.map(c => {
                        let shopId;
                        this.store.select(getCarShopsStoreState).pipe(
                            map(carShops => {
                                if (!carShops.shops || !carShops.shops.length) return;
                                shopId = carShops.shops.find(shop => shop.cars.includes(c._id));
                            }),
                        );
                        return {
                            img: c.img,
                            title: c.brand,
                            shopId: shopId,
                            carId: c._id,
                            model: c.model,
                        };
                    });
                }),
            ),
            this.store.select(getCarShopsStoreState).pipe(
                map(s => {
                    if (!s.shops || !s.shops.length) return;
                    return s.shops.map(shop => {
                        return {
                            img: shop.imgShop,
                            title: shop.name,
                            shopId: shop._id,
                        };
                    });
                }),
            ),
        ).pipe(
            map(([cars, shops]) => {
                if (!shops || !shops.length || !cars || !cars.length) return;
                return shops.concat(cars);
                // return [...shops, ...cars];
            }));
    }

    onClick(item): void {
        if (!item.carId) {
            this.router.navigate([`/shop/${item.shopId}`]);
        }
        else {
            this.router.navigate([`/car-info/${item.shopId}/${item.carId}`]);
        }
        this.searchString = '';
    }

    onBlur(): void {
        this.searchString = '';
    }

}
