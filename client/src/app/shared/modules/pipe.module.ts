import { SearchPipe } from './../../shared/pipes/search.pipe';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [SearchPipe],
  exports: [SearchPipe],
})
export class PipeModule { }
