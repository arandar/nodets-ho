export interface ISearch {
    carId?: string;
    shopId?: string;
    model?: string;
    img: string;
    title: string;
}