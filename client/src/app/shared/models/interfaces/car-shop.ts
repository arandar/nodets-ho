import { ICar } from './car';

export interface ICarShop {
    _id?: string;
    imgShop: string;
    name: string;
    cars: string[];
}