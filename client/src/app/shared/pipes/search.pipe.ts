import { Pipe, PipeTransform } from '@angular/core';
import { ISearch } from '../models/interfaces/search';

@Pipe({
    name: 'search',
    pure: false,
})

export class SearchPipe implements PipeTransform {
    // создаем статическую функцию
    static filter(items: ISearch[], term: string): ISearch[] {
        // присваиваем значениям строки поиска всегда нижний регистр
        const toCompare = term.toLowerCase();
        // вместо any поставил интерфейс, чтобы потом можно выбрать по чем фильтровать
        // сейчас ищет по всем свойствам
        return items.filter((item: ISearch) => {
            if (!item || !item.title && !item.model) return;
            if (item.model) {
                return item.model.toString().toLowerCase().includes(toCompare) ||
                 item.title.toString().toLowerCase().includes(toCompare);
            }
            return item.title.toString().toLowerCase().includes(toCompare);
            // можно было не писать, так как по дефолту, но пускай будет
        });
    }
    // есть два значения: items - получаемые объекты и terms - значение в строке поиска
    transform(items: ISearch[], term: string): ISearch[] {
        // при пустом значении в строке поиска выводим все существующие объекты
        // можно еще добавить || !items, когда будет возможность искать по картинкам, к примеру
        if (!term) return items;
        // в ином случае выводит отфильтрованные значения
        return SearchPipe.filter(items, term);
    }
}