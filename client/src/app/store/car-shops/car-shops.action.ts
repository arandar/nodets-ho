import { ICarShop } from './../../shared/models/interfaces/car-shop';
import { Action } from '@ngrx/store';

export enum CarShopsTypes {
    LoadCarShops = '[CAR-SHOP] load car shops [...]',
    LoadCarShopsSuccess = '[CAR-SHOP] load car shops [SUCCESS]',
    LoadCarShopsError = '[CAR-SHOP] load car shops [ERROR]',
    AddCarToShop = '[CAR-SHOP] add car to shop [...]',
    DeleteCarFromShop = '[CAR-SHOP] delete car from shop [...]',
}

// LOAD CAR SHOPS
export class LoadCarShopsAction implements Action {
    readonly type = CarShopsTypes.LoadCarShops;
}

export class LoadCarShopsSuccessAction implements Action {
    readonly type = CarShopsTypes.LoadCarShopsSuccess;
    constructor(public payload: ICarShop[]) { }
}

export class LoadCarShopsErrorAction implements Action {
    readonly type = CarShopsTypes.LoadCarShopsError;
    constructor(public payload: any) { }
}

export class AddCarToShopAction implements Action {
    readonly type = CarShopsTypes.AddCarToShop;
    constructor(public payload: { shopId: string, carId: string }) { }
}

export class DeleteCarFromShopAction implements Action {
    readonly type = CarShopsTypes.DeleteCarFromShop;
    constructor(public payload: string) { }
}

// тут экспортируются типы
export type CarShopsActions
    = LoadCarShopsAction
    | LoadCarShopsSuccessAction
    | LoadCarShopsErrorAction
    | AddCarToShopAction
    | DeleteCarFromShopAction
    ;