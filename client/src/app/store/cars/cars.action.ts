import { Action } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';

export enum CarsTypes {
    LoadCars = '[CAR] load cars [...]',
    LoadCarsSuccess = '[CAR] load cars [SUCCESS]',
    LoadCarsError = '[CAR] load cars [ERROR]',
    AddCar = '[CAR-SHOP] add car [...]',
    AddCarSuccess = '[CAR-SHOP] add car [SUCCESS]',
    AddCarError = '[CAR-SHOP] add car [ERROR]',
    EditCar = '[CAR-SHOP] edit car [...]',
    EditCarSuccess = '[CAR-SHOP] edit car [SUCCESS]',
    EditCarError = '[CAR-SHOP] edit car [ERROR]',
    DeleteCar = '[CAR-SHOP] delete car [...]',
    DeleteCarSuccess = '[CAR-SHOP] delete car [SUCCESS]',
    DeleteCarError = '[CAR-SHOP] delete car [ERROR]',
}

// LOAD CAR SHOPS
export class LoadCarsAction implements Action {
    readonly type = CarsTypes.LoadCars;
}

export class LoadCarsSuccessAction implements Action {
    readonly type = CarsTypes.LoadCarsSuccess;
    constructor(public payload: ICar[]) { }
}

export class LoadCarsErrorAction implements Action {
    readonly type = CarsTypes.LoadCarsError;
    constructor(public payload: any) { }
}

// ADD CAR
export class AddCarAction implements Action {
    readonly type = CarsTypes.AddCar;
    constructor(public payload: { shopId: string, car: ICar }) { }
}

export class AddCarSuccessAction implements Action {
    readonly type = CarsTypes.AddCarSuccess;
    constructor(public payload: { car: ICar }) { }
}

export class AddCarErrorAction implements Action {
    readonly type = CarsTypes.AddCarError;
    constructor(public payload: any) { }
}

// EDIT CAR
export class EditCarAction implements Action {
    readonly type = CarsTypes.EditCar;
    constructor(public payload: ICar) { }
}

export class EditCarSuccessAction implements Action {
    readonly type = CarsTypes.EditCarSuccess;
    constructor(public payload: ICar) { }
}

export class EditCarErrorAction implements Action {
    readonly type = CarsTypes.EditCarError;
    constructor(public payload: any) { }
}

// DELETE CAR
export class DeleteCarAction implements Action {
    readonly type = CarsTypes.DeleteCar;
    constructor(public payload: string) { }
}

export class DeleteCarSuccessAction implements Action {
    readonly type = CarsTypes.DeleteCarSuccess;
    constructor(public payload: string) { }
}

export class DeleteCarErrorAction implements Action {
    readonly type = CarsTypes.DeleteCarError;
    constructor(public payload: any) { }
}


// тут экспортируются типы
export type CarsActions
    = LoadCarsAction
    | LoadCarsSuccessAction
    | LoadCarsErrorAction
    | AddCarAction
    | AddCarSuccessAction
    | AddCarErrorAction
    | EditCarAction
    | EditCarSuccessAction
    | EditCarErrorAction
    | DeleteCarAction
    | DeleteCarSuccessAction
    | DeleteCarErrorAction
    ;