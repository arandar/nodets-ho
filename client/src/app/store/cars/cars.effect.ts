import { AddCarToShopAction, DeleteCarFromShopAction } from './../car-shops/car-shops.action';
import { HttpService } from './../../shared/services/http.service';
import { switchMap, map, tap, catchError, mergeMap } from 'rxjs/operators';
import { of, Observable, empty } from 'rxjs';
import { Injectable } from '@angular/core';
import { Effect, ofType, Actions } from '@ngrx/effects';
import {
    CarsActions, CarsTypes, LoadCarsAction, LoadCarsSuccessAction,
    LoadCarsErrorAction, AddCarAction, AddCarSuccessAction, AddCarErrorAction,
    EditCarSuccessAction, EditCarAction, EditCarErrorAction, DeleteCarSuccessAction,
    DeleteCarAction, DeleteCarErrorAction,
} from './cars.action';

@Injectable()
export class CarsEffects {

    @Effect()
    loadCars$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadCarsAction>(CarsTypes.LoadCars),
        switchMap(() => {
            return this.http.getCars().pipe(
                map((res: any) => new LoadCarsSuccessAction(res)),
                catchError(error => of(new LoadCarsErrorAction(error))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<LoadCarsErrorAction>
            (CarsTypes.LoadCarsError),
        map(action => action.payload),
        tap(e => {
            console.error(e);
            // this.errorService.toConsole(e);
        }),
    );

    @Effect()
    addCar$: Observable<CarsActions> = this.actions$.pipe(
        ofType<AddCarAction>(CarsTypes.AddCar),
        switchMap((value) => {
            return this.http.addCar(value.payload.shopId, value.payload.car).pipe(
                mergeMap((res: any) => {
                    return of(new AddCarSuccessAction({ car: { ...value.payload.car, _id: res } }),
                        new AddCarToShopAction({ shopId: value.payload.shopId, carId: res }));
                }),
                catchError(err => of(new AddCarErrorAction(err))),
            );
        }),
    );

    @Effect({ dispatch: false })
    onAddError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<AddCarErrorAction>
            (CarsTypes.AddCarError),
        map(action => action.payload),
        tap(e => {
            console.error(e);

        }),
    );

    @Effect()
    editCar$: Observable<CarsActions> = this.actions$.pipe(
        ofType<EditCarAction>(CarsTypes.EditCar),
        switchMap((value) => {
            return this.http.editCar(value.payload).pipe(
                mergeMap((res: any) => {
                    return of(new EditCarSuccessAction({ ...value.payload }));
                }),
                catchError(err => of(new EditCarErrorAction(err))),
            );
        }),
    );

    @Effect({ dispatch: false })
    editCarError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<EditCarErrorAction>
            (CarsTypes.EditCarError),
        map(action => action.payload),
        tap(e => {
            console.error(e);

        }),
    );

    @Effect()
    deleteCar$: Observable<CarsActions> = this.actions$.pipe(
        ofType<DeleteCarAction>(CarsTypes.DeleteCar),
        map(action => action.payload),
        switchMap(carId => {
            return this.http.deleteCar(carId).pipe(
                mergeMap((res: any) => {
                    return of(new DeleteCarSuccessAction(carId),
                        new DeleteCarFromShopAction(carId));
                }),
                catchError(err => of(new DeleteCarErrorAction(err))),
            );
        }),
    );

    @Effect({ dispatch: false })
    deleteCarError$: Observable<CarsActions> = this.actions$.pipe(
        ofType<DeleteCarErrorAction>
            (CarsTypes.DeleteCarError),
        map(action => action.payload),
        tap(e => {
            console.error(e);

        }),
    );

    constructor(
        private actions$: Actions,
        private http: HttpService,
    ) { }
}