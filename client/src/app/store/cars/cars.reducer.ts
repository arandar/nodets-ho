// import { ICarShop } from './../../shared/models/interfaces/car-shop';
import { CarsActions, CarsTypes } from './cars.action';
import { createFeatureSelector } from '@ngrx/store';
import { ICar } from '../../shared/models/interfaces/car';

export class CarsState {
    cars: ICar[];
    isCarsLoading: boolean = true;
}

export function reducer(state = new CarsState(), action: CarsActions): CarsState {
    switch (action.type) {
        // LOAD CARS
        case CarsTypes.LoadCars:
            return {
                ...state,
                isCarsLoading: true,
            };
        case CarsTypes.LoadCarsSuccess:
            return {
                ...state,
                cars: action.payload,
                isCarsLoading: false,
            };
        case CarsTypes.LoadCarsError:
            return {
                ...state,
                isCarsLoading: false,
                cars: [],
            };

        // ADD CARS
        case CarsTypes.AddCar:
            return {
                ...state,
            };

        case CarsTypes.AddCarSuccess:
            return {
                ...state,
                cars: [
                    ...state.cars,
                    action.payload.car,
                ],
            };

        case CarsTypes.AddCarError:
            return {
                ...state,
            };

        // EDIT CAR
        case CarsTypes.EditCar:
            return {
                ...state,
            };

        case CarsTypes.EditCarSuccess:
            return {
                ...state,
                cars: [
                    ...state.cars.map(car => {
                        if (car._id != action.payload._id) return car;
                        return { ...action.payload };
                    }),
                ],
            };

        case CarsTypes.EditCarError:
            return {
                ...state,
            };

        // DELETE CAR
        case CarsTypes.DeleteCar:
            return {
                ...state,
            };

        case CarsTypes.DeleteCarSuccess:
            return {
                ...state,
                cars: [
                    ...state.cars.filter(car => car._id !== action.payload),
                ],
            };

        case CarsTypes.DeleteCarError:
            return {
                ...state,
            };

        default: {
            return state;
        }
    }
}

export const getCarsStoreState = createFeatureSelector<CarsState>('cars');