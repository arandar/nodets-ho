import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ICar } from '../../../shared/models/interfaces/car';

@Component({
    selector: 'shop-item',
    templateUrl: './shop-item.component.html',
    styleUrls: ['./shop-item.component.scss']
})
export class ShopItemComponent implements OnInit {
    @Input() car: ICar;
    @Output() editClick: EventEmitter<any> = new EventEmitter();
    @Output() deleteClick: EventEmitter<string> = new EventEmitter();

    constructor() { }

    ngOnInit() { }

    onEditClick(carId: number): void {
        this.editClick.emit(carId);
    }
    onDeleteClick(carId: string, event: Event): void {
		this.deleteClick.emit(carId)
		event.stopPropagation();
    }

}
