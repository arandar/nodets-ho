import { DeleteCarAction } from './../../store/cars/cars.action';
import { ICar } from './../../shared/models/interfaces/car';
import { getCarsStoreState } from './../../store/cars/cars.reducer';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { map } from 'rxjs/operators';
import { getCarShopsStoreState } from './../../store/car-shops/car-shops.reducer';
import { ICarShop } from '../../shared/models/interfaces/car-shop';
import { Observable, Subscription, combineLatest } from 'rxjs';
// import { DataService } from '../../shared/services/data.service';

@Component({
    selector: 'car-test-shop',
    templateUrl: './shop.component.html',
    styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
    shopId;
    shop$: Observable<ICarShop>;
    cars$: Observable<ICar[]>;
    sub: Subscription;
    constructor(
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.shop$ = combineLatest(
            this.activatedRoute.params.pipe(map(p => this.shopId = p.id)),
            this.store.select(getCarShopsStoreState).pipe(map(s => s.shops)),
        ).pipe(
            map(([id, shops]) => {
                if (!id || !shops || !shops.length) return;
                return shops.find(s => s._id === id);
            }),
        );

        this.cars$ = combineLatest(
            this.shop$,
            this.store.select(getCarsStoreState).pipe(map(s => s.cars)),
        ).pipe(
            map(([shop, cars]) => {
                if (!shop || !cars || !shop.cars || !shop.cars.length || !cars.length) return;
                return cars.filter(c => shop.cars.includes(c._id));
            }),
        );

    }

    onClick(i: number): void {
        this.router.navigate([`/car-info/${this.shopId}/${i}`]);
    }

    onAddClick(): void {
        this.router.navigate([`/edit/${this.shopId}`]);
    }

    onEditClick(i): void {
        this.router.navigate([`/edit/${this.shopId}/${i}`]);
    }

    onDeleteClick(i): void {
        this.store.dispatch(new DeleteCarAction(i));
    }

    goHome(): void {
        this.router.navigate([`/home`]);
    }

}
