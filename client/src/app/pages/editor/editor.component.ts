import { HttpService } from './../../shared/services/http.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/index';
import { ICar } from '../../shared/models/interfaces/car';
import { map } from 'rxjs/operators';
import { AddCarAction, EditCarAction } from '../../store/cars/cars.action';
import { getCarsStoreState } from '../../store/cars/cars.reducer';
import { Location } from '@angular/common';

@Component({
    selector: 'car-test-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class EditorComponent implements OnInit, OnDestroy {
    shopId;
    carId;
    editorMode: boolean = false;
    carForm: FormGroup;
    subscription: Subscription;
    data$: Observable<ICar>;

    file: File;
    pathImg: string;
    urlImg: string;

    constructor(
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private store: Store<AppState>,
        private httpService: HttpService,
        private location: Location,
        // private dataService: DataService
    ) { }

    ngOnInit(): void {
        // подписка на параметры передаваемые в url
        this.subscription = this.activatedRoute.params.subscribe((p) => {
            this.shopId = p.shopId;
            this.carId = p.carId;
            // если параметры переданы, то включается режим редактирования, которые изменяет название формы
            if (p.shopId && p.carId) {
                this.editorMode = true;
                this.data$ = this.store.select(getCarsStoreState).pipe(
                    map(s => {
                        // if (!s.cars) return;
                        // const shop = s.shops.find(s => s._id == p.shopId);
                        const car = s.cars && s.cars.find(c => c._id == p.carId);
                        // инициализируется форма со значениями из наденного выше объекта
                        this.initForm(car);
                        return car;
                    }));
            } else {
                this.initForm();
            }
            // в переменную data$ записываются данные из store

        });
    }

    initForm(car?: ICar): void {
        if (car) {
            this.carForm = this.fb.group({
                img: [car.img, [Validators.required]],
                color: [car.color, [Validators.required]],
                model: [car.model, [Validators.required]],
                price: [car.price, [Validators.required]],
                brand: [car.brand, [Validators.required]],
                age: [car.age, [Validators.required]],
                description: [car.description, [Validators.required]],
            });
        } else {
            this.carForm = this.fb.group({
                img: [''],
                color: ['', [Validators.required]],
                model: ['', [Validators.required]],
                price: ['', [Validators.required]],
                brand: ['', [Validators.required]],
                age: ['', [Validators.required]],
                description: ['', [Validators.required]],
            });
        }

    }

    // отправка изображения на бэк, в ответку получим имя файла на бэке уже
    uploadImg(): Promise<string> {
        try {
            if (this.file == undefined) return Promise.resolve(undefined);
            return this.httpService.uploadPublic(this.file).toPromise();
        } catch (error) {
            // this.notificationService.snackbar('Uploading error');
            return Promise.resolve(undefined);
        }
    }
    // само событие при клике кнопки загрузки изображения. срабатывает после выбора файлы.
    async loadFile(event): Promise<string> {
        // вытягивается имя загруженного файла. имя настоящее, то есть как у пользователя на ПК
        // можно проверить в консоли  console.log(file);
        const file = event.target.files[0];
        if (!file) {
            return this.file = undefined;
        }
        // присываем переменной file значение константы file
        this.file = file;
    }

    async onSubmit(): Promise<string> {
        if (!this.carForm.valid) return;
        if (this.editorMode) {
            // отправка изображения
            this.pathImg = await this.uploadImg();
            if (this.pathImg !== undefined) {
                // формируем урл, по которому лежит наше изображение не бэке
                this.urlImg = `http://localhost:3000/public/${this.pathImg}`;
                // присваиваем это значение полю формы img
                this.carForm.controls.img.setValue(this.urlImg);
            }

            this.editCar(this.carForm.value);
        } else {
            // отправка изображения
            this.pathImg = await this.uploadImg();
            // console.log(this.pathImg);
            if (this.pathImg === null || this.pathImg === undefined) return;
            // формируем урл, по которому лежит наше изображение не бэке
            this.urlImg = `http://localhost:3000/public/${this.pathImg}`;
            // присваиваем это значение полю формы img
            this.carForm.controls.img.setValue(this.urlImg);
            // console.log(this.carForm.controls.img.value);
            this.addCar(this.carForm.value);
        }
        this.router.navigate([`/shop/${this.shopId}`]);
    }


    addCar(carId): void {
        this.store.dispatch(new AddCarAction(
            {
                shopId: this.activatedRoute.params['value'].shopId,
                car: carId,
            },
        ));
    }

    editCar(carId): void {
        this.store.dispatch(new EditCarAction(
            {
                ...carId,
                _id: this.carId,
            },
        ));
    }

    goHome(): void {
        this.location.back();
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
